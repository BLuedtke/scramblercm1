// ScramblerCM1.cpp : Diese Datei enthält die Funktion "main". Hier beginnt und endet die Ausführung des Programms.
//

#include <iostream>
#include <filesystem>
#include <sstream>
#include <opencv2/opencv.hpp>
using std::cout;
using std::cerr;
using std::endl;
using namespace cv;

void test1();

int main()
{
	cout << "Start" << endl;
	try
	{
		cv::Mat I = cv::imread("images/img2.jpg");
		//uchar* table = new uchar();
		int divideWith = 0;
		std::cin >> divideWith;
		if (!divideWith)
		{
			cout << "Invalid number entered for dividing. " << endl;
			return -1;
		}

		uchar table[256];
		for (int i = 0; i < 256; ++i)
			table[i] = (uchar)(divideWith * (i / divideWith));
		// accept only char type matrices
		CV_Assert(I.depth() == CV_8U);

		int channels = I.channels();

		int nRows = I.rows;
		int nCols = I.cols * channels;

		if (I.isContinuous())
		{
			nCols *= nRows;
			nRows = 1;
		}

		uchar* p;
		int count = 0;
		for (int i = 0; i < nRows; ++i)
		{
			p = I.ptr<uchar>(i);
			for (int j = 0; j < nCols; ++j)
			{
				count++;
				p[j] = table[p[j]];
				cout << (float)p[j] << " ";

			}
			cout << "\n";
		}
		cout << "Elements in p: " << count << endl;
		for (int i = 0; i < count; i++) {
			//cout << p[i] << " ";
			if (i % nCols == 0) {
				//cout << "\n";
			}
		}
		//delete[] table;
		//return I;
		/*
		cout << img.at<int>(0,0,0) << endl;
		uchar* imgdata = img.data;

		for (int j = 0; j < img.rows; j++) {
			//sum += std::max(imgdata[j], 0.);
			for (int i = 0; i < img.cols; i++) {
				//cout << img.at<float>(j,i) << endl;
			}
		}
		cout << "cols: " << img.cols << endl;
		/**/
	}
	catch (const std::exception& e)
	{
		cerr << e.what() << "\n";
	}
}
void test1() {
	try
	{
		std::filesystem::path t = std::filesystem::current_path();
		cout << t << endl;
		cv::Mat img = cv::imread("images/img.jpg");
		cv::namedWindow("image", cv::WINDOW_NORMAL);
		imshow("image", img);
		cv::waitKey(0);
	}
	catch (const std::exception& e)
	{
		cerr << e.what() << "\n";
	}
}



// Programm ausführen: STRG+F5 oder Menüeintrag "Debuggen" > "Starten ohne Debuggen starten"
// Programm debuggen: F5 oder "Debuggen" > Menü "Debuggen starten"

// Tipps für den Einstieg: 
//   1. Verwenden Sie das Projektmappen-Explorer-Fenster zum Hinzufügen/Verwalten von Dateien.
//   2. Verwenden Sie das Team Explorer-Fenster zum Herstellen einer Verbindung mit der Quellcodeverwaltung.
//   3. Verwenden Sie das Ausgabefenster, um die Buildausgabe und andere Nachrichten anzuzeigen.
//   4. Verwenden Sie das Fenster "Fehlerliste", um Fehler anzuzeigen.
//   5. Wechseln Sie zu "Projekt" > "Neues Element hinzufügen", um neue Codedateien zu erstellen, bzw. zu "Projekt" > "Vorhandenes Element hinzufügen", um dem Projekt vorhandene Codedateien hinzuzufügen.
//   6. Um dieses Projekt später erneut zu öffnen, wechseln Sie zu "Datei" > "Öffnen" > "Projekt", und wählen Sie die SLN-Datei aus.
